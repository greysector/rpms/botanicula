Summary: Point and click exploration and adventure game
Name: botanicula
Version: 2.0.0.2
Release: 3
URL: https://www.gog.com/game/botanicula
Group:  Applications/Games
Source0: https://cdn-hw.gog.com/secure/botanicula/linux/gog_botanicula_%{version}.sh
Source1: %{name}.desktop
Source2: %{name}.sh
License: GOG
BuildRequires: bsdtar
BuildArch: noarch
# Required to switch to noarch package
Obsoletes: %{name} < 2.0.0.2-1
Requires: adobe-air

%description
Botanicula is a point'n'click exploration game created by Jaromír Plachý and
Amanita Design. It’s about a group of five friends -- little tree creatures who
set out on a journey to save the last seed from their home tree, which is
infested by evil parasites. The gameplay is about exploration, solving little
funny puzzles, meeting strange tree creatures, and paying attention to the music
as it provides you with useful clues and hints. The whole game takes place on
one huge and strange tree that is inhabited by bizarre and freaky creatures.

This is not an ordinary game. It is far from it. Everything about it is
curiously unique and very surreal, and that alone makes it worth checking out.
It's just not possible to be bored in this game, as every new location brings
something new and intriguing. The exploration elements are made with incredible
polish and attention to detail, and the same applies to the musical score, sound
effects, and, most importantly, the visuals. The synergy of those elements
create a beautiful and memorable adventure that is enthralling beyond measure.

* Visually stunning and emotionally touching game that is as unique and bizarre
  as it is fun.
* Highly interactive environment that encourages experimentation and
  exploration.
* Stylized soundtrack and sound effects, created by Czech alternative band DVA,
  are beautifully synchronized with what is happening on the screen.
* Special Botanicula language.
    
%prep
%setup -qcT
bsdtar -x -f%{S:0}

%install
install -dm755 %{buildroot}{%{_bindir},%{_datadir}/%{name}}
cp -pr data/noarch/game/bin/{*.{swf,xml},data} %{buildroot}%{_datadir}/%{name}/
install -pm755 %{S:2} %{buildroot}%{_bindir}/%{name}

for i in 16 32 36 48 72 128 256 512 ; do
    install -Dpm644 data/noarch/game/bin/data/icons/b${i}.png \
        %{buildroot}%{_datadir}/icons/hicolor/${i}x${i}/apps/%{name}.png
done
desktop-file-install --dir %{buildroot}%{_datadir}/applications %{S:1}

%files
%license "data/noarch/docs/End User License Agreement.txt"
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/%{name}

%changelog
* Tue Apr 14 2020 Dominik Mierzejewski <rpm@greysector.net> 2.0.0.2-3
- use full paths in desktop file
- provide own launcher
- chrpath not required for build anymore

* Mon Apr 13 2020 Dominik Mierzejewski <rpm@greysector.net> 2.0.0.2-2
- unbundle Adobe AIR runtime
- switch to noarch

* Wed Mar 18 2020 Dominik Mierzejewski <rpm@greysector.net> 2.0.0.2-1
- initial build
